#include <iostream>
#include <time.h>
#include <cstdlib>
#include <string>

using namespace std;

class Karta {
public:
	int kolor;
	int numer;


	void tworzJednaKarte(int k, int nr) {
		this->kolor = k;
		this->numer = nr;
	}

	Karta tworzKarte(int k, int nr) {
		Karta kartonik;
		kartonik.kolor = k;
		kartonik.numer = nr;
		return kartonik;
	}
	void wyswietlKarte() {
		string kolory[] = { "\x6" ,"\x4" ,"\x3" ,"\x5" };
		string numery[] = { "","A","2","3","4","5","6","7","8","9","10","W","D","K" };
		cout << numery[(this->numer)] << kolory[(this->kolor)] << " ";
	}

};


class Talia : public Karta {
public:
	Karta talia[52];
	bool uzyta[52];

	void tworzTalie() {
		for (int i = 0; i < 52; i++) {
			talia[i] = tworzKarte(i % 4, i % 13 + 1);
		}
	}

	void wyswietlTalie() {
		for (int i = 0; i < 52; i++) {
			talia[i].wyswietlKarte();
		}
	}
};


class Gracz : public Karta {
private:
	string imie;
	Karta reka[8];
public:
	void zmienImie(string name) {
		this->imie = name;
	}

	string pokazImie() {
		return this->imie;
	}

	void losujReke(Talia t) {

		int random;
		int i = 0;
		do {
			random = rand() % 52;
			while (t.uzyta[random] != true) {
				reka[i] = t.talia[random];
				t.uzyta[random] = true;
				i++;
			}
		} while (i < 8);
	}

	void pokazReke() {
		for (int i = 0; i < 8; i++) {
			reka[i].wyswietlKarte();
		}
	}
};

int main() {
	srand(time(NULL));
	Talia t;
	t.tworzTalie();

	int n;

	cout << "Podaj liczbe graczy (od 2 do 5): ";
	cin >> n;
	if (n < 2) n = 2;
	if (n > 5) n = 5;

	cout << "Liczba graczy: " << n << endl;
	Gracz *gracze = new Gracz[n];
	for (int i = 0; i < n; i++) {
		string imie;
		cout << "Podaj imie " << i+1 << " gracza: ";
		cin >> imie;
		gracze[i].zmienImie(imie);
		cout << endl;
		gracze[i].losujReke(t);
	}

	cout << "Wylosowane karty:" << endl;

	for (int i = 0; i < n; i++) {
		cout << gracze[i].pokazImie() << ": \t"; gracze[i].pokazReke();
		cout << endl;
	}

	cout << endl;
	system("pause");

	return 0;
}